/*
 * cubesketch v0.1
 * Copyright 2012 John Wood
 *
 * Arduino 1.0 sketch for 4x4x4 LED_CUBE
 *
 */

#include <SPI.h>
#include <LEDCube.h>
#include <TimerOne.h>

#define BUTTON_PIN 4
#define NUM_ANIMATIONS 8

// Animations
void drip();
void bee();
void rotate();
void blinky();
void layers();
void equalizer();
void onoff();
void randomAnimation();
void (*animations[NUM_ANIMATIONS])(void) = {
    drip, 
    bee, 
    rotate,
    blinky, 
    layers, 
    equalizer,
    onoff,
    randomAnimation};
int animationIterator = 0;
boolean startOver = true;

volatile boolean buttonPress = false;

void setup()
{
    // Initialize the LED cube.
    int anodes[4] = {5, 6, 7, 8};
    Cube.initialize(9, anodes);
    Cube.startRefresh(1); // Refresh rate of 1ms.

    // Initialize Timer1 to handle out button presses.
    pinMode(BUTTON_PIN, INPUT);
    Timer1.initialize(15000); // 15ms
    Timer1.attachInterrupt(buttonCheckISR);

    randomSeed(analogRead(2));
}


void loop()
{
    if (buttonPress)
    {
        buttonPress = false;
        Cube.clear();
        animationIterator = (animationIterator + 1) % NUM_ANIMATIONS;
        startOver = true;
    }

    animations[animationIterator]();
    startOver = false;
}

void buttonCheckISR()
{
    static boolean wait = false;
    static int lastState = HIGH;

    int state = digitalRead(BUTTON_PIN);

    if (LOW == state)
    {
        if (LOW == lastState)
        {
            if (wait)
            {
                // Ok, should be debounced.
                wait = false;
                buttonPress = true;
            }
        }
        else // was HIGH
        {
            wait = true;           
        } 
    }
    lastState = state;
}

void drip()
{
    if (startOver)
    {
        for (int x=0; x<4; ++x)
        {
            for (int y=0; y<4; ++y)
            {
                Cube.ledOn(x, y, 3);
            }
        }
    }

    int x = random(4);
    int y = random(4);

    if (Cube.getState(x, y, 3))
    {
        for (int z=3; z>0; --z)
        {
            Cube.ledOff(x, y, z);
            Cube.ledOn(x, y, z-1);
            delay(100);
        }
    }
    else
    {
        for (int z=0; z<3; ++z)
        {
            Cube.ledOff(x, y, z);
            Cube.ledOn(x, y, z+1);
            delay(100);
        }
    }
}

void blinky()
{
    int x = random(4);
    int y = random(4);
    int z = random(4);

    Cube.toggleLed(x, y, z);
    delay(100);
}

void bee()
{
    static int x = 0;
    static int y = 0;
    static int z = 0;

    if (startOver)
    {
        x = 0;
        y = 0;
        z = 0;
    }

    Cube.ledOff(x, y, z);

    for (;;)
    {
        int dir = random(6);
        switch (dir)
        {
            case 0: // up
                if (z == 3)
                {
                    continue;
                }
                z += 1;
                break;
            case 1: // down
                if (z == 0)
                {
                    continue;
                }
                z -= 1;
                break;
            case 2: // left
                if (x == 0)
                {
                    continue;
                }
                x -= 1;
                break;
            case 3: // right
                if (x == 3)
                {
                    continue;
                }
                x += 1;
                break;
            case 4: // forward
                if (y == 3)
                {
                    continue;
                }
                y += 1;
                break;
            case 5: // back
                if (y == 0)
                {
                    continue;
                }
                y -= 1;
                break;
        }
        break;
    }

    Cube.ledOn(x, y, z);

    delay(50);
}

void rotate()
{
    if (startOver)
    {
        int toggle = 0;
        for (int z=0; z<4; ++z)
        {
            for (int x=0; x<4; ++x)
            {
                for (int y=0; y<4; ++y)
                {
                    if (z%2 == toggle)
                    {
                        Cube.ledOn(x, y, z);
                    }
                }
                toggle = toggle ? 0 : 1;
            }
            toggle = toggle ? 0 : 1;
        }
        delay(100);
        return;
    }

    for (int x=0; x<4; ++x)
    {
        for (int y=0; y<4; ++y)
        {
            for (int z=0; z<4; ++z)
            {
                if (Cube.getState(x, y, z))
                {
                    Cube.ledOff(x, y, z); 
                }
                else
                {
                    Cube.ledOn(x, y, z);
                }
            }
        }
    }
    delay(100);
}

void layers()
{
    static int plane = 0; // x=0, y=1, z=2
    static int pos = 0;
    static boolean goingDown = false;
    if (startOver)
    {
        plane = 0;
        pos = 0;
        goingDown = false;
    }

    for (int a=0; a<4; ++a)
    {
        for (int b=0; b<4; ++b)
        {
            switch (plane)
            {
                case 0:
                    goingDown ? 
                        Cube.ledOff(pos, a, b): 
                        Cube.ledOn(pos, a, b);
                    break;
                case 1:
                    goingDown ?
                        Cube.ledOff(a, pos, b):
                        Cube.ledOn(a, pos, b);
                    break;
                case 2:
                    goingDown ?
                        Cube.ledOff(a, b, pos):
                        Cube.ledOn(a, b, pos);
                    break;
            }
        }
    }

    delay(100);

    if (goingDown)
    {
        if (pos == 0)
        {
            plane = (plane+1)%3;
            goingDown = false;
        }
        else
        {
            --pos;
        }
    }
    else 
    {
        if (pos == 3)
        {
            goingDown = true;
        }
        else
        {
            ++pos;
        }
    }
}

void equalizer()
{
    if (startOver)
    {
        for (int x=0; x<4; ++x)
        {
            for (int y=0; y<4; ++y)
            {
                Cube.ledOn(x, y, 0);
            }
        }
    }

    int x = random(4);
    int y = random(4);
    int z = random(4);
         
    for (int tmp=1; tmp<4; ++tmp)
    {
        (tmp <= z) ? Cube.ledOn(x, y, tmp) : Cube.ledOff(x, y, tmp);
    }

    delay(100);
}

void onoff()
{
    static boolean turningOn = true;

    if (startOver)
    {
        turningOn = true;
    }

    boolean quit = false;

    for (int x=0; x<4; ++x)
    {
        for (int y=0; y<4; ++y)
        {
            for (int z=0; z<4; ++z)
            {
                unsigned char ledOn = Cube.getState(x, y, z);
                // if we are turning everything on, but there is still
                // a light out...
                if (turningOn && !ledOn)
                {
                    quit = true;
                }
                // or if we are turning everything off, but there is still
                // a light on...
                else if (!turningOn && ledOn)
                {
                    quit = true;
                }
            }
            if (quit)
            {
                break;
            }
        }
        if (quit)
        {
            break;
        }
    }

    if (!quit)
    {
        // then we either turned all on or off
        turningOn = !turningOn;
    }

    for (;;)
    {
        int x = random(4);
        int y = random(4);
        int z = random(4);
    
        int ledOn = Cube.getState(x, y, z);

        if (turningOn && ledOn)
        {
            continue;
        }
        else if (!turningOn && !ledOn)
        {
            continue;
        }

        turningOn ? Cube.ledOn(x, y, z) : Cube.ledOff(x, y, z);
        break;
    }


    delay(50);
}

void randomAnimation()
{
    static unsigned long displayTime = 5000;
    static int i = random(4);
    static unsigned long switchTime = millis() + displayTime;

    if (startOver)
    {
        i = random(4);
        switchTime = millis() + displayTime;
    }

    if (millis() >= switchTime)
    {
        startOver = true;
        switchTime = millis() + displayTime;
        i = random(4);
        Cube.clear();
    }    

    animations[i]();
    startOver = false;
}
